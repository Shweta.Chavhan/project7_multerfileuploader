FROM node:13-alpine

EXPOSE 6163

WORKDIR /app

RUN npm install

COPY . .

COPY package*.json ./

ENTRYPOINT ["node","server.js"]
