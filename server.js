const express = require("express");
const multer = require("multer");
const ejs = require("ejs");
const path = require("path");
const app = express();

app.set("views", path.join(__dirname, "/views"));
app.set("view engine", "ejs"); //to use any othre template engine just replace the name

const port = process.env.PORT || 6163;

//setting up static directory
app.use(express.static(__dirname + "/public"));

app.listen(port, () => {
  console.log(`server is running at localhost:${port}`);
});

//Name : /
//Access : public
//@Description : Home Page
app.get("/", (req, res) => {
  res.render("index");
});

const storage = multer.diskStorage({
  destination: function(req, file, cb) {
    cb(null, "./public/uploads");
  },
  filename: function(req, file, cb) {
    const uniqueSuffix = Date.now() + path.extname(file.originalname); //To creating unique filename
    cb(null, file.fieldname + "-" + uniqueSuffix);
  }
});

const upload = multer({ storage: storage, limits: "5MB" }).single("avatar");

app.post("/upload", (req, res) => {
  console.log(req.file);
  upload(req, res, error => {
    if (error) res.render("index", { message: error });
    else {
      res.render("index", {
        message: "Uploaded successfully",
        filename: `uploads/${req.file.filename}`
      });
    }
  });
});
